package luxoft.codingchallange.service;

import luxoft.codingchallange.filesystem.File;
import luxoft.codingchallange.filesystem.FileSystem;
import luxoft.codingchallange.filesystem.Permissions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SearchFileSystemService {

    private File[][] files = FileSystem.files;

    public List<File> searchFiles(Map<String, Object> attributes) {
        List<File> fileList = new ArrayList<>();

        if(files == null || files.length == 0)
            return fileList;

        int rowNum = files.length;
        int colNum = files[0].length;

        for(int i=0;i<rowNum + colNum - 1;i++) {
            int line = 0;
            int column = i;

            if( i >= (colNum - 1)) {
                line = i - (rowNum - 1);
                column = colNum - 1;
            }

            while (line < rowNum && column > -1) {
                boolean isExists = isFileMatchWithAttributes(files[line][column], attributes);
                if(isExists)
                    fileList.add(files[line][column]);

                line++;
                column--;
            }
        }

        return fileList;
    }

    public boolean isFileMatchWithAttributes(File file, Map<String, Object> attr) {

        String attrName = (String)attr.get("name");
        Long attrSize = (Long) attr.get("size");
        Permissions attrPermissions = (Permissions) attr.get("permissions");
        LocalDateTime attrTimeCreated = (LocalDateTime)attr.get("timeCreated");


        boolean isSameName = (attrName == null || file.getName().contains(attrName));
        boolean isSameSize = (attrSize == null || attrSize == file.getSize());
        boolean isSamePermission = (attrPermissions == null || attrPermissions.equals(file.getPermission()));
        boolean isBeforeTimeCreated = (attrTimeCreated == null || file.getTimeCreated().isBefore(attrTimeCreated));

        return isSameName && isSameSize && isSamePermission && isBeforeTimeCreated;
    }

}
