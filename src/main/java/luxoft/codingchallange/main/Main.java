package luxoft.codingchallange.main;

import luxoft.codingchallange.filesystem.File;
import luxoft.codingchallange.filesystem.Permissions;
import luxoft.codingchallange.service.SearchFileSystemService;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args){
        //TODO search for files
        //TODO print list of files found for previous search

        // I had to make some assumptions:
        // 1. All the attributes that you want to search for need to be filled.
        // 2. There is no OR in the search, e.g.: findByNameOrSize.
        // 3. I am considering null as true value, because the values you don't want to search are null.
        // e.g.: only by name, all other attributes will be null.

        SearchFileSystemService search = new SearchFileSystemService();

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("size", null);
        attributes.put("name", null);
        attributes.put("timeCreated", LocalDateTime.of(2022, 05, 18, 00, 00)); // 18-05-2022
        attributes.put("permissions", Permissions.W);


        List<File> files = search.searchFiles(attributes);

        for(File file : files) {
            System.out.println(file);
        }

    }
}
