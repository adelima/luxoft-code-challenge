package luxoft.codingchallange.service;

import luxoft.codingchallange.filesystem.File;
import luxoft.codingchallange.filesystem.Permissions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SearchFileSystemServiceTest {

    private SearchFileSystemService searchService = new SearchFileSystemService();
    private Map<String, Object> attributes;

    @BeforeEach
    public void init()
    {
        attributes = new HashMap<>();
    }

    @Test
    public void searchNonExistentFile()
    {
        attributes.put("name", "asd");

        Assertions.assertEquals(Collections.EMPTY_LIST, searchService.searchFiles(attributes));
    }


    @Test
    public void searchExistentFile()
    {
        attributes.put("name", "file");

        Assertions.assertNotEquals(Collections.EMPTY_LIST, searchService.searchFiles(attributes));

    }

    @Test
    public void isFileMatchWithAttributes()
    {
        File file = new File(1000L, "test-file.txt", LocalDateTime.of(2022, 05, 17, 00, 00), Permissions.R);

        attributes.put("size", 1000L);
        attributes.put("name", "test-file.txt");
        attributes.put("timeCreated", LocalDateTime.of(2022, 05, 18, 00, 00));
        attributes.put("permissions", Permissions.R);

        Assertions.assertTrue(searchService.isFileMatchWithAttributes(file, attributes));
    }

    @Test
    public void isFileDoesNotMatchWithAttributes()
    {
        File file = new File(1000L, "test-file.txt", LocalDateTime.of(2022, 05, 17, 00, 00), Permissions.R);

        attributes.put("size", 2000L);
        attributes.put("name", "test-file.txt");
        attributes.put("timeCreated", LocalDateTime.of(2022, 05, 18, 00, 00));
        attributes.put("permissions", Permissions.R);

        Assertions.assertFalse(searchService.isFileMatchWithAttributes(file, attributes));

    }
}
